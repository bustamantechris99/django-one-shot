from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList, TodoItem
from .forms import CreateForm, UpdateForm, CreateItemForm, UpdateItemForm


def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {
      'todolist': todolist
    }
    return render(request, 'todos/display.html', context)


def todo_list_detail(request, id):
    todolist = TodoList.objects.get(id=id)

    context = {
      'todolist': todolist
    }

    return render(request, 'todos/detail.html', context)


def todo_list_create(request):
    if request.method == 'POST':
        form = CreateForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', id=list.id)
    else:
        form = CreateForm()
    context = {
      'form': form
      }
    return render(request, 'todos/create.html', context)


def todo_list_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = UpdateForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = UpdateForm(instance=list)
    context = {
        'list_object': list,
        'form': form
      }
    return render(request, 'todos/update.html', context)


def todo_list_delete(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
      'list_object': list
      }
    if request.method == 'GET':
        return render(request, 'todos/delete.html', context)
    else:
        list.delete()
        return redirect('todo_list_list')


def todo_item_create(request):
    if request.method == 'POST':
        form = CreateItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect('todo_list_detail', id=item.list.id)
    else:
        form = CreateItemForm()
    context = {
      'form': form
    }
    return render(request, 'todos/create_list.html', context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = UpdateItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=id)
    else:
        form = UpdateItemForm(instance=item)
    context = {
      'form': form
    }
    return render(request, 'todos/update_list.html', context)
