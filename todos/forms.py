from .models import TodoList, TodoItem
from django.forms import ModelForm, TextInput


class CreateForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ['name']
        widgets = {
            'title': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Name'
                })}


class UpdateForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ['name']
        widgets = {
            'title': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'Name'
                })}


class CreateItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ['task', 'due_date', 'is_completed', 'list']
        widgets = {
            'due_date': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'year-month-day'
                })}

class UpdateItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ['task', 'due_date', 'is_completed', 'list']
        widgets = {
            'due_date': TextInput(attrs={
                'class': "form-control",
                'style': 'max-width: 300px;',
                'placeholder': 'year-month-day'
                })}